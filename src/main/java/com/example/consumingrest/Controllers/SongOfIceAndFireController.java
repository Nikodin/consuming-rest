package com.example.consumingrest.Controllers;

import com.example.consumingrest.Repo.SongOfIceAndFireAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("api/v1/targaryen")
public class SongOfIceAndFireController {

    @Autowired
    SongOfIceAndFireAPI songOfIceAndFireAPI;

    @GetMapping
    public ResponseEntity<String> getTargaryenTitles()  {
        try {
            return new ResponseEntity<>(songOfIceAndFireAPI.getHouseTargaryenTitles(), HttpStatus.ACCEPTED);
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
}
