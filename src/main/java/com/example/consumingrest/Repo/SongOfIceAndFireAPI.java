package com.example.consumingrest.Repo;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class SongOfIceAndFireAPI {

    public String getHouseTargaryenTitles() throws IOException {

        URL url = new URL("https://anapioficeandfire.com/api/houses/378");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                content.append(inputLine);
            }
            br.close();

            JSONObject targaryenTitles = new JSONObject(content.toString());

            var titles = targaryenTitles.getJSONArray("titles").toString();

            return titles;

        }

        return null;
    }
}


